package com.bengkel.booking.services;

import java.util.List;

import com.bengkel.booking.models.BookingOrder;
import com.bengkel.booking.models.Car;
import com.bengkel.booking.models.Customer;
import com.bengkel.booking.models.ItemService;
import com.bengkel.booking.models.MemberCustomer;
import com.bengkel.booking.models.Vehicle;

public class PrintService {
	
	public static void printMenu(String[] listMenu, String title) {
		String line = "+---------------------------------+";
		int number = 1;
		String formatTable = " %-2s. %-25s %n";
		
		System.out.printf("%-25s %n", title);
		System.out.println(line);
		
		for (String data : listMenu) {
			if (number < listMenu.length) {
				System.out.printf(formatTable, number, data);
			}else {
				System.out.printf(formatTable, 0, data);
			}
			number++;
		}
		System.out.println(line);
		System.out.println();
	}
	
	public static void printVechicle(List<Vehicle> listVehicle) {
		String formatTable = "| %-2s | %-15s | %-15s | %-15s | %-15s | %-5s | %-15s |%n";
		String line = "+----+-----------------+------------+-----------------+-----------------+-------+-----------------+%n";
		System.out.println("List Kendaraan:");
		System.out.format(line);
	    System.out.format(formatTable, "No", "Vechicle Id", "Warna", "Brand", "Transmisi", "Tahun", "Tipe Kendaraan");
	    System.out.format(line);
	    int number = 1;
	    String vehicleType = "";
	    for (Vehicle vehicle : listVehicle) {
	    	if (vehicle instanceof Car) {
				vehicleType = "Mobil";
			}else {
				vehicleType = "Motor";
			}
	    	System.out.format(formatTable, number, vehicle.getVehiclesId(), vehicle.getColor(), vehicle.getBrand(), vehicle.getTransmisionType(), vehicle.getYearRelease(), vehicleType);
	    	number++;
	    }
	    System.out.printf(line);
	}
	
	//Silahkan Tambahkan function print sesuai dengan kebutuhan.
	public static void printCustomer(Customer customer){
		String customerStatus = customer instanceof MemberCustomer ? "Member" : "Non Member";
		System.out.println(customerStatus);
		System.out.println("Customer Profile");
		String formatPrint = "%-15s %-10s";

		System.out.printf( formatPrint, "Customer id: " ,customer.getCustomerId());
		System.out.println("");
		System.out.printf( formatPrint ,"Nama: ", customer.getName());
		System.out.println("");
		System.out.printf(formatPrint, "Cutomer Status", customerStatus );
		System.out.println("");
		System.out.printf(formatPrint,"Alamat: " ,customer.getAddress());
		System.out.println("");
		if (customer instanceof MemberCustomer) {
			System.out.printf(formatPrint,"Saldo Coin: " ,((MemberCustomer)customer).getSaldoCoin());
			System.out.println("");
		}
	}

	public static void printItemService(List<ItemService> listItemService, Vehicle selectedVehicle){
		String formatTable = "| %-2s | %-15s | %-15s | %-15s | %-15s |%n";
		String line = "+----+-----------------+------------+-----------------+-----------------+-------+-----------------+%n";
		System.out.println("List Kendaraan:");
		System.out.format(line);
		System.out.printf(formatTable,"No", "Service Id", "Name Service", "Tipe Kendaraan", "Harga");
		System.out.format(line);
		int number = 1;
		for (ItemService itemService : listItemService) {
			if (itemService.getVehicleType().equals(selectedVehicle.getVehicleType())) {
				System.out.format(formatTable, number, itemService.getServiceId(), itemService.getServiceName(), itemService.getVehicleType(), itemService.getPrice());
				number++;
			}
		}
		System.out.printf(line);

	}

	public static void printBookingOrder(List<BookingOrder> listABookingOrders){
		String formatTable = "| %-2s | %-20s | %-15s | %-15s | %-15s | %-15s | %-15s |%n";
		String line = "+----+-----------------+------------+-----------------+-----------------+-------+-----------------+%n";
		System.out.println("List Kendaraan:");
		System.out.format(line);
	    System.out.format(formatTable, "No", "Booking Id", "Nama", "Payment", "Total Service", "Total Payment", "List Service");
	    System.out.format(line);
	    int number = 1;
	    for (BookingOrder bookingOrder : listABookingOrders) {
			System.out.format(formatTable, number, bookingOrder.getBookingId(), bookingOrder.getCustomer().getName(), bookingOrder.getPaymentMethod(),
								bookingOrder.getTotalServicePrice(), bookingOrder.getTotalPayment(), printService(bookingOrder.getServices()));
		}
		System.out.format(line);
	}
	
	public static String printService(List<ItemService> listItemServices){
		String result = "";

		for (ItemService itemService : listItemServices) {
			result += itemService.getServiceName() + ", ";
		}
		return result;
	}
}
