package com.bengkel.booking.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

import com.bengkel.booking.models.BookingOrder;
import com.bengkel.booking.models.Customer;
import com.bengkel.booking.models.ItemService;
import com.bengkel.booking.models.MemberCustomer;
import com.bengkel.booking.models.Vehicle;

public class BengkelService {
	private static int idCounter = 1;
	//Silahkan tambahkan fitur-fitur utama aplikasi disini
	
	//Login
	public static void userLogin(List<Customer> listAllCustomers){
		String custID, custPassword;
		Scanner scanner = new Scanner(System.in);
		Optional<Customer> foundCustID;
		Customer selectedCustomer;
		boolean password;
		int counter = 0;
		do{
			System.out.print("Masukan Customer ID : ");
			custID = scanner.nextLine();
			foundCustID = getCustomer(listAllCustomers, custID);
			selectedCustomer = foundCustID.orElse(null);
			if (selectedCustomer == null) {
				System.out.println("Customer ID tidak ditemukan");
				counter++;
			}

			if (counter == 3) {
				System.exit(counter);
			}
			
		}while(selectedCustomer == null);

		counter = 0;
		do{
			System.out.print("Masukan Password : ");
			custPassword = scanner.nextLine();
			password = checkCustPassword(selectedCustomer, custPassword);

			if (!password) {
				System.out.println("Password Salah");
				counter++;
			}

			if (counter == 3) {
				System.exit(counter);
			}

		}while (!password);

		MenuService.mainMenu(selectedCustomer);

		
	}
	
	//Info Customer
	public static void infoCustomer(Customer customer){
		PrintService.printCustomer(customer);
		PrintService.printVechicle(customer.getVehicles());
	}

	//Booking atau Reservation
	public static void booking(List<ItemService> listAllItemService, Customer selectedCustomer, List<BookingOrder> listAllBookingOrders){
		Scanner scanner = new Scanner(System.in);
		String vehicleID,serviceID, bookingID, paymentMethod;
		String idBook = String.format("-%03d", idCounter);
		String formatPrint = "%-15s %-10s";
		double servicePrice = 0;
		double currentSaldo;
		Optional<Vehicle> foundVehicleID;
		Vehicle selectedVehicle;
		List<Vehicle> listVehicle = selectedCustomer.getVehicles();
		List<ItemService> listItemServicetoUse = new ArrayList<>();
		ItemService selectedItemService  = new ItemService();
		BookingOrder bookingOrder = new BookingOrder();
		int maxItemService;
		boolean payment = false;

		if (selectedCustomer instanceof MemberCustomer) {
			maxItemService = 2;
		}else{
			maxItemService = 1;
		}

		do{
			System.out.print("Masukan Vehicle Id : ");
			vehicleID = scanner.nextLine();
			foundVehicleID = getVehicle(listVehicle, vehicleID);
			selectedVehicle = foundVehicleID.orElse(null);
		}while(selectedVehicle == null);

		PrintService.printItemService(listAllItemService, selectedVehicle);

		outerloop:
        do{
            do{
                System.out.print("Silahkan Masukkan Service Id : ");
                serviceID = scanner.nextLine();
                selectedItemService = getItemServiceByServiceID(listAllItemService, serviceID);
				if (listItemServicetoUse.size() >= maxItemService) {
					System.out.println("Pemilihan Item Service sudah maksimal");
					break;
				}
                if (selectedItemService != null) {
                    if (!listItemServicetoUse.contains(selectedItemService)) {
                        listItemServicetoUse.add(selectedItemService);
						servicePrice = servicePrice + selectedItemService.getPrice();
                    }else{
                        System.out.println("item service sudah dipilih");
                    }
                    
                }else{
                    System.out.println("Tidak ada Service dengan ID tersebut");
                }
                
            }while (selectedItemService == null);

            do{
                System.out.println("Ingin pilih service lain (Y/T)");
                serviceID = scanner.nextLine();
                if (serviceID.equalsIgnoreCase("T")) {
                    break outerloop;
                }else if (serviceID.equalsIgnoreCase("Y")) {
                    continue outerloop;
                }
            }while (serviceID.isEmpty() || !serviceID.equalsIgnoreCase("Y") || !serviceID.equalsIgnoreCase("T"));
        }while(true);

		do{
			System.out.println("Pilih metode pembayaran (Saldo Coin atau Cash): ");
			paymentMethod = scanner.nextLine();
			if (!(selectedCustomer instanceof MemberCustomer)) {
				if (paymentMethod.equalsIgnoreCase("Cash")) {
					System.out.println("Booking berhasil");
					payment = true;
					break;
				}else{
					System.out.println("metode pembayaran tidak sesuai");
				}
			}else{
				if (paymentMethod.equalsIgnoreCase("Cash") || paymentMethod.equalsIgnoreCase("Saldo Coin")) {
					System.out.println("Booking berhasil");
					payment = true;
					break;
				}
			}
		}while (!payment);

		bookingID = "Book-" + selectedCustomer.getCustomerId() + idBook;
		idCounter++;

		bookingOrder.setBookingId(bookingID);
		bookingOrder.setCustomer(selectedCustomer);
		bookingOrder.setPaymentMethod(paymentMethod);
		bookingOrder.setTotalServicePrice(servicePrice);
		bookingOrder.setServices(listItemServicetoUse); 

		bookingOrder.calculatePayment();

		if (selectedCustomer instanceof MemberCustomer && paymentMethod.equalsIgnoreCase("Saldo Coin")) {
			currentSaldo = ((MemberCustomer)selectedCustomer).getSaldoCoin() - bookingOrder.getTotalPayment();
			((MemberCustomer)selectedCustomer).setSaldoCoin(currentSaldo);
		}

		listAllBookingOrders.add(bookingOrder);
		System.out.printf(formatPrint, "Total Harga Service : ", bookingOrder.getTotalServicePrice());
		System.out.println("");
		System.out.printf(formatPrint, "Total Pembayaran : ", bookingOrder.getTotalPayment());
		System.out.println("");
	}

	//Top Up Saldo Coin Untuk Member Customer
	public static void topUpSaldoCoin(Customer selectedCustomer){
		Scanner scanner = new Scanner(System.in);
		double saldoTopUp, currentSaldo;
		if (selectedCustomer instanceof MemberCustomer) {
			System.out.print("Masukan Saldo Top Up : ");
			saldoTopUp = scanner.nextDouble();
			currentSaldo = ((MemberCustomer)selectedCustomer).getSaldoCoin() + saldoTopUp;
			((MemberCustomer)selectedCustomer).setSaldoCoin(currentSaldo);
		}else{
			System.out.println("Maaf fitur ini hanya untuk Member saja!");
		}
	}

	//Logout
	public static void logout(List<BookingOrder> listAllBookingOrders){
		listAllBookingOrders.clear();
	}



	public static Optional<Customer> getCustomer(List<Customer> listAllCustomers, String custID){
		return listAllCustomers.stream()
					.filter(customer -> customer.getCustomerId().equals(custID))
					.findFirst();
	}

	public static boolean checkCustPassword(Customer customer, String custPassword){
		return customer.getPassword().equals(custPassword);
	}
	
	public static Optional<Vehicle> getVehicle(List<Vehicle> listVehicles, String vehicleID){
		return listVehicles.stream()
						.filter(vehicle -> vehicle.getVehiclesId().equals(vehicleID))
						.findFirst();
	}

	public static ItemService getItemServiceByServiceID(List<ItemService> serviceList, String serviceID){
        ItemService serviceChoices = null;
        for (ItemService service : serviceList) {
            if (service.getServiceId().equalsIgnoreCase(serviceID)) {
                serviceChoices =  service;
                break;
            }
        }
        return serviceChoices;
    }
}
