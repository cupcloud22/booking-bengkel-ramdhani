package com.bengkel.booking.services;

import java.util.ArrayList;
import java.util.List;
// import java.util.Scanner;

import com.bengkel.booking.models.BookingOrder;
import com.bengkel.booking.models.Customer;
import com.bengkel.booking.models.ItemService;
import com.bengkel.booking.repositories.CustomerRepository;
import com.bengkel.booking.repositories.ItemServiceRepository;

public class MenuService {
	private static List<Customer> listAllCustomers = CustomerRepository.getAllCustomer();
	private static List<ItemService> listAllItemService = ItemServiceRepository.getAllItemService();
	private static List<BookingOrder> listAllBookingOrders = new ArrayList<>();
	// private static Scanner input = new Scanner(System.in);
	private static Customer customer;
	public static void run() {
		boolean isLooping = true;
		do {
			login();
			mainMenu(customer);
		} while (isLooping);
		
	}
	
	public static void login() {
		String[] listMenuLogin = {"Login", "Exit"};
		int menuChoice = 0;
		boolean isLooping = true;

		do{
			PrintService.printMenu(listMenuLogin, "Login");
			menuChoice = Validation.validasiNumberWithRange("Masukan Pilihan Menu : ", "Input Harus Berupa Angka", "^[0-9]+$", listMenuLogin.length-1, 0);
			System.out.println();

			switch (menuChoice) {
				case 1:
					BengkelService.userLogin(listAllCustomers);
					break;
				case 0:
					System.out.println("Sampai jumpa!");
					System.exit(0);
				default:
					System.out.println("Inputan tidak valid, coba lagi!");
				}
		}while (isLooping);
	}
	
	public static void mainMenu(Customer selectedCustomer) {
		String[] listMenu = {"Informasi Customer", "Booking Bengkel", "Top Up Bengkel Coin", "Informasi Booking", "Logout"};
		int menuChoice = 0;
		boolean isLooping = true;
		
		do {
			PrintService.printMenu(listMenu, "Booking Bengkel Menu");
			menuChoice = Validation.validasiNumberWithRange("Masukan Pilihan Menu:", "Input Harus Berupa Angka!", "^[0-9]+$", listMenu.length-1, 0);
			System.out.println(menuChoice);
			
			switch (menuChoice) {
			case 1:
				BengkelService.infoCustomer(selectedCustomer);
				break;
			case 2:
				BengkelService.booking( listAllItemService, selectedCustomer, listAllBookingOrders);
				break;
			case 3:
				BengkelService.topUpSaldoCoin(selectedCustomer);
				break;
			case 4:
				PrintService.printBookingOrder(listAllBookingOrders);
				break;
			default:
				System.out.println("Logout");
				BengkelService.logout(listAllBookingOrders);
				isLooping = false;
				break;
			}
		} while (isLooping);
		
		
	}
	
	//Silahkan tambahkan kodingan untuk keperluan Menu Aplikasi
}
